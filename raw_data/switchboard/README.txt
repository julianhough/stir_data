Switchboard Disfluency Annotated (SWDA) Corpora and Tools

This is a combination of the SWDA scripts available from Christopher Pott's 2011 Computational Pragmatics course (http://compprag.christopherpotts.net/swda.html) with the disfluency marked up in the Penn Tree Bank release, meaning it is a comprehensive combination of both annotation mark ups. The annotations and scripts here are usable on Windows, Mac and Unix systems.

This contains various resources in the subdirectories:

* swda: The main SWDA (dialogue act and disfluency annotated) corpus files. This has been modified in places manually where disfluency annotation was inconsistent. swda.py in the /scripts sub-directory is the file with the python objects for the corpus in the

* swda_divisions_disfluency_detection: The list of division of files for the Switchboard disfluency detection task, splitting into training, heldout and test data, standardly used since Johnson and Charniak 2004. The SWDisfTrainSW2_ranges file lists the files not in the Penn Tree Bank but which are still marked up for disfluency, not standardly used in the training, but can be.

* swda_tree_pos_maps: Mappings from words in swda to their leaf position in the Penn TreeBank files or their POS tag position in the POS sequence if in the non Penn TreeBank files. Use the python scripts included in /scripts to load these as python objects in your code.

* repair_annotations: The intersection of the original SWDA mark up and the Penn Tree Bank mark-up in tab-separated .csv format. All of the repair annoations are in the format:

conversation_no,reparandum_start,reparandum_end,interregnum_start,interregnum_end,repair_start,repair_end,embedded,position

4103	8	4	8	6	8	6	8	7	Reparandum.	p1	

These terms are self-explanatory if you look at the Switchboard disfluency annotation manual (Taylor et al. 1995). The 'embedded' field indicates whether this repair is embedded in an element of another repair. This is often blank. The repair_annotations.csv file contains all the other files in this directory concatenated together.

* derived_corpora: 
        -cleaned_of_disfluency: Files cleaned of disfluency for language model training (in the spirit of the noisy-channel model)
	-disfluency_detection: Files for the standard Switchboard disfluency detection tasks, produced by disfluency_detection_corpus.py in the /scripts folder. Both in the partial words included (_partial) and partial words excluded settings. These are produced from the annotations in /repair_annotations and the script scripts/disfluency_detection_corpus.py as explained above.
	-edit_term: Files with the edit term occurrences only from the Switchboard disfluency marked up corpora